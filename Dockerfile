FROM node:alpine
COPY package*.json /home/
RUN cd /home && npm ci
COPY . /home/
WORKDIR /home
CMD node .