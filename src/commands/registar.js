const Discord = require("discord.js");
const { PLATFORMS } = require("../constants");
const { isValidURL } = require("../utils");

const utils = require("../utils");

module.exports = {
  name: "registar",
  aliases: ["r"],
  description:
    "Regista o teu link para receberes pagamentos.\nExemplo: `.registar [plataforma] <link>`",
  async execute(client, message) {
    const channel = message.channel;
    const msgArray = message.content.split(" ").filter((el) => el !== "");

    const platform = msgArray[1];
    const link = msgArray[2];

    if (!platform || !Object.keys(PLATFORMS).includes(platform)) {
      await channel.send(
        "É para registar o quê? A tua mãe de 4?\n`.registar [plataforma] <link>`"
      );
      await channel.send(
        "Por enquanto só dão estas: " + Object.keys(PLATFORMS).join(", ")
      );
      return;
    }

    if (!link || !isValidURL(link)) {
      await channel.send(
        "Bom link bronco. Não a sério, bom link.\n`.registar [plataforma] <link>`"
      );
      return;
    }

    await client.bank.registerUser(message.author.id, platform, link);

    const embed = new Discord.EmbedBuilder();

    embed.setTitle("👤 Utilizador registado");
    embed.setColor(Discord.Colors.DarkGreen);
    embed.setTimestamp();
    embed.setDescription(`Link para ${PLATFORMS[platform]}: ${link}`);

    await channel.send({embeds: [embed]});

    return true;
  },
};
