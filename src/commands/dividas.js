const Discord = require("discord.js");
const { bold } = require("@discordjs/builders");

const utils = require("../utils");
const { payButton } = require("../components");

module.exports = {
  name: "dividas",
  aliases: ["d"],
  description: "Lista as minhas dívidas\nExemplo: `.dívidas`",
  async execute(client, message) {
    let channel = message.channel;
    let debts = await client.bank.getDebts(message.author.id);
    const embed = new Discord.EmbedBuilder();
    if (debts.length != 0) {
      embed.setTitle("💶 Dívidas: ");
      embed.setColor(Discord.Colors.Red);

      let embedDescription = "";
      let creditors = [];
      debts.forEach((creditor) => {
        const name = utils.getUserNickname(creditor.creditor, message);
        creditors.push({ label: name, description: creditor.amount + "€", value: creditor.creditor + "|" + creditor.amount, emoji: "↘️" });
        embedDescription += `▫️ ${bold(name)} > ${creditor.amount}€\n`;
      });
      embed.setDescription(embedDescription);
      await payButton(client, embed, message, creditors);

    } else {
      embed.setTitle("Sem dívidas :)");
      embed.setColor(Discord.Colors.DarkGreen);

      await channel.send({ embeds: [embed] });
    }

    return true;
  },
};
