const { hyperlink } = require("@discordjs/builders");
const Discord = require("discord.js");
const { PLATFORMS } = require("../constants");
const { isValidURL } = require("../utils");

const utils = require("../utils");

module.exports = {
  name: "test",
  aliases: ["t"],
  description: "Test command",
  async execute(client, message) {
    const channel = message.channel;

    await channel.send("Acknowledged");

    return true;
  },
};
