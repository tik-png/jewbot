const { hyperlink } = require("@discordjs/builders");
const Discord = require("discord.js");
const { PLATFORMS } = require("../constants");
const utils = require("../utils");

module.exports = {
  name: "deves",
  aliases: ["dev"],
  description:
    "Adiciona um valor à divida dos utilizadores especificados.\nExemplo: `.deves @user 420.69`",
  async execute(client, message) {
    const channel = message.channel;
    let value = utils.getValueFromMessage(message);
    const desc = utils.getDescriptionFromMessage(message);

    const debtors = Array.from(message.mentions.users.keys());
    const creditor = message.author;

    // Handle message with no debt value
    if (!value) {
      await channel.send("E o valor, nabo da merda?\n`.deves @users [valor]`");
      return;
    }
    value = Math.round(value * 1e2) / 1e2;

    // Handle message without any users tagged
    if (!debtors || !debtors.length) {
      await channel.send(
        "Quem é que te deve isso? O ar?\n`.deves @users [valor]`"
      );
      return;
    }

    // Can't add debt to self
    if (debtors.includes(creditor.id)) {
      await channel.send("Não podes dever a ti próprio, lul");
      return;
    }

    let embedDescription = "";

    await Promise.all(
      debtors.map(async (debtorId) => {
        const newDebt = await client.bank.newDebt(
          creditor.id,
          debtorId,
          value,
          desc
        );

        const debtDetails = await client.bank.getDebt(creditor.id, debtorId);

        embedDescription += utils.formatMessage(
          utils.getUserNickname(debtDetails.debtor, message),
          "deve",
          debtDetails.amount,
          utils.getUserNickname(debtDetails.creditor, message),
          value,
          desc
        );
      })
    );

    const platformLinks = await client.bank.getUserLinks(message.author.id);

    if (platformLinks) {
      Object.entries(platformLinks).forEach(([platform, link]) => {
        embedDescription += `\nPagar por ${hyperlink(
          PLATFORMS[platform],
          link,
          `Pagar por ${PLATFORMS[platform]}`
        )}`;
      });
    }

    const embed = new Discord.EmbedBuilder();

    embed.setTitle("🏦 Novas dívidas:");
    embed.setColor(Discord.Colors.Red);
    embed.setTimestamp();
    embed.setDescription(embedDescription);
    await channel.send({embeds: [embed]});

    return true;
  },
};
