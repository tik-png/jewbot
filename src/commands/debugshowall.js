const Discord = require("discord.js");
const utils = require("../utils");

module.exports = {
  name: "debug showall",
  aliases: ["debug"],
  description: "Lista as dívidas todas",
  async execute(client, message) {
    let channel = message.channel;
    let debts = await client.bank.getAllDebts();

    if (debts.length != 0) {
      const embed = new Discord.EmbedBuilder();
      embed.setTitle("🖥️ Dívidas");
      embed.setColor(Discord.Colors.Blue);
      let embedDescription = "";
      debts.forEach((entry) => {
        embedDescription += `▫️ **${utils.getUserNickname(
          entry.debtor,
          message
        )}** deve a **${utils.getUserNickname(entry.creditor, message)}** > ${
          entry.amount
        }€\n`;
      });
      embed.setDescription(embedDescription);
      await channel.send({embeds: [embed]});
    }

    return true;
  },
};
