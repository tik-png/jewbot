const Discord = require("discord.js");

module.exports = {
  name: "help",
  aliases: ["h", "c", "ajuda", "comandos", "commands"],
  description: "Lista todos os comandos.",
  async execute(client, message) {
    const prefix = client.configs.get("prefix");
    const channel = message.channel;
    const embed = new Discord.EmbedBuilder()
      .setTitle("📕 Comandos disponíveis: ")
      .setColor(Discord.Colors.Blue);
    client.commands.forEach((command) => {
      const description =
        (command.aliases ? `*${command.aliases.map(a => prefix + a).join(", ")}*\n` : "") +
        command.description;
      embed.addFields({name: prefix + command.name, value: description, inline: false});
    });

    await channel.send({embeds: [embed]});
    return true;
  }
};
