const { paginateBoard } = require("../components");

module.exports = {
  name: "logdividas",
  aliases: ["logd"],
  description: "Histórico de dívidas.\nExemplo: `.logdividas [@creditor] [@debtor]`",
  async execute(client, message) {
    const [creditor, debtor] = Array.from(message.mentions.users.keys());
    paginateBoard(client, message, "getDebtLogs", creditor, debtor);

    return true;
  },
};
