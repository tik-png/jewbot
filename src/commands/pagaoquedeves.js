const Discord = require('discord.js');
const utils = require('../utils');

module.exports = {
  name: 'pagaoquedeves',
  aliases: ['paga'],
  description: 'Avisa os devedores :)\nExemplo: `.pagaoquedeves`',
  async execute(client, message) {
    let channel = message.channel;
    let debtors = await client.bank.getDebtors(message.author.id);
    const embed = new Discord.EmbedBuilder();
    let mentions = '';

    if (debtors.length != 0) {
      embed.setTitle('⚠️ Estes retards devem-te: ');
      embed.setColor(Discord.Colors.Red);
      let embedDescription = '';
      debtors.forEach((debtor) => {
        const name = utils.getUserNickname(debtor.debtor, message);
        mentions += `<@${debtor.debtor}>`;
        embedDescription += `▫️ **${name}** > ${debtor.amount}€\n`;
      });
      embed.setDescription(embedDescription);
    } else {
      embed.setTitle('Ninguém te deve');
      embed.setColor(Discord.Colors.DarkGreen);
    }

    await channel.send({ content: mentions ? '||' + mentions + '||' : '', embeds: [embed] });
    return true;
  },
};
