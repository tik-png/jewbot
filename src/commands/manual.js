const Discord = require("discord.js");
const utils = require("../utils");

module.exports = {
  name: "manual",
  aliases: ["m"],
  description:
    "O user 1 paga ao user 2 (debug).\nExemplo: `.manual @user1 @user2 5.67`",
  async execute(client, message) {
    const channel = message.channel;
    let value = utils.getValueFromMessage(message);
    const desc = utils.getDescriptionFromMessage(message);
    const users = Array.from(message.mentions.users.keys());

    // Handle message with no debt value
    if (!value) {
      await channel.send(
        "E o valor, nabo da merda?\n`.manual @user1 @user2 5.67`"
      );
      return;
    }
    value = Math.round(value * 1e2) / 1e2;

    // Handle message without any users tagged
    if (!users || users.length < 2) {
      await channel.send(
        "Input inválido.\nExemplo: `.manual @user1 @user2 5.67`"
      );
      return;
    }

    await client.bank.makePayment(users[0], users[1], value, desc);

    const embed = new Discord.EmbedBuilder();
    embed.setTitle("📲 Transferência manual efetuada:");
    embed.setColor(Discord.Colors.DarkGreen);
    embed.setTimestamp();

    let embedDescription = utils.formatMessage(
      utils.getUserNickname(users[0], message),
      "pagou",
      value,
      utils.getUserNickname(users[1], message),
      null,
      desc
    );

    embed.setDescription(embedDescription);

    await channel.send({embeds: [embed]});

    return true;
  },
};
