const { paginateBoard } = require("../components");

module.exports = {
  name: "logpagamentos",
  aliases: ["logp"],
  description: "Histórico de pagamentos.\nExemplo: `.logpagamentos [@de] [@para]`",
  async execute(client, message) {
    const [from, to] = Array.from(message.mentions.users.keys());
    paginateBoard(client, message, "getPaymentLogs", from, to);

    return true;
  },
};
