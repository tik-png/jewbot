const Discord = require("discord.js");
const utils = require("../utils");

const EMOJIS = ["🥇", "🥈", "🥉"];

module.exports = {
  name: "halloffame",
  aliases: ["hof"],
  description: "Mostra a Hall of Fame das dívidas (só o top 10).\nExemplo: `.halloffame`",
  async execute(client, message) {
    const channel = message.channel;

    let debts = await client.bank.getAllDebts();

    const hallfOfFame = debts.reduce((acc, curr) => {
      const debtor = curr.debtor;
      if (!acc[debtor]) acc[debtor] = curr.amount;
      else acc[debtor] += curr.amount;
      return acc;
    }, {});

    if (!hallfOfFame) {
      await channel.send("Deu merda 💩");
      return;
    }

    const sorted = Object.entries(hallfOfFame).sort(
      ([, amountA], [, amountB]) => amountB - amountA
    );

    const embed = new Discord.EmbedBuilder();
    embed.setTitle("🏆 Hall of Fame");
    embed.setColor(Discord.Colors.Yellow);
    let embedDescription = "";

    sorted.slice(0,10).forEach((entry, index) => {
      embedDescription += `${EMOJIS[index] || ""} **${utils.getUserNickname(
        entry[0],
        message
      )}**: ${Math.round(entry[1] * 1e2) / 1e2}\n`;
    });

    embed.setDescription(embedDescription);
    await channel.send({embeds: [embed]});

    return true;
  }
};
