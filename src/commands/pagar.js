const Discord = require("discord.js");
const utils = require("../utils");

module.exports = {
  name: "pagar",
  aliases: ["p"],
  description:
    "Paga um valor ao(s) utilizador(es) específicados.\nExemplo: `.pagar @users 12.34`",
  async execute(client, message, creditors = null, value = null, desc = null) {
    const channel = message.channel;
    value = value ? value : utils.getValueFromMessage(message);
    desc = desc ? desc : utils.getDescriptionFromMessage(message);
    creditors = creditors ? creditors : Array.from(message.mentions.users.keys());
    const debtor = message.author;

    const embed = new Discord.EmbedBuilder();
    let embedDescription = "";

    embed.setTitle("💸 Pagamento(s) efetuado(s):");
    embed.setColor(Discord.Colors.DarkGreen);
    embed.setTimestamp();

    // Handle message with no debt value
    if (!value) {
      await channel.send("E o valor, nabo da merda?\n`.pagar @users [valor]`");
      return;
    }
    value = Math.round(value * 1e2) / 1e2;

    // Handle message without any users tagged
    if (!creditors || !creditors.length) {
      await channel.send(
        "A quem é que vais pagar? O ar?\n`.pagar @users [valor]`"
      );
      return;
    }

    // Can't pay to self
    if (creditors.includes(debtor.id)) {
      await channel.send("Não podes pagar a ti próprio, lul");
      return;
    }

    await Promise.all(
      creditors.map(async (creditorId) => {
        await client.bank.makePayment(debtor.id, creditorId, value);
        const toUsername = utils.getUserNickname(creditorId, message);

        embedDescription += utils.formatMessage(
          utils.getUserNickname(debtor.id, message),
          "pagou",
          value,
          toUsername,
          null,
          desc
        );
      })
    );

    embed.setDescription(embedDescription);
    await channel.send({embeds: [embed]});

    await client.commands.get("dividas").execute(client, message);

    return true;
  },
};
