const Discord = require("discord.js");

const packageJson = require('../../package.json');
const version = packageJson.version;

module.exports = {
  name: "version",
  aliases: ["v"],
  description: "Mostra a versão do bot.\nExemplo: `.version`",
  async execute(client, message) {
    const channel = message.channel;

    await channel.send(`🖥️ jewbot version: \`${version}\``);

    return true;
  },
};
