require("dotenv").config();
const Discord = require("discord.js");
const fs = require("fs");
var path = require("path");
const Bank = require("./database.js");
const configs = require(path.join(__dirname, "..", "config.json"));

// Intents config (https://discordjs.guide/popular-topics/intents.html)
const intents = new Discord.IntentsBitField();
configs.intents.forEach((element) =>
  intents.add(Discord.IntentsBitField.Flags[element])
);
const client = new Discord.Client({ intents });

// Connect to mongodb
const bank = new Bank(process.env.DB_CONNSTRING);
(async () => {
  await bank.connect();
})();
client.bank = bank;

// Load configs
client.configs = new Map();
Object.keys(configs).forEach(function (key) {
  client.configs.set(key, configs[key]);
});

// Load commands
client.commands = new Discord.Collection();
const commandFiles = fs
  .readdirSync("./src/commands/")
  .filter((file) => file.endsWith(".js"));
for (const file of commandFiles) { // never touch this for or else
  const command = require(`./commands/${file}`);
  client.commands.set(command.name, command);
}

// Load events
const eventFiles = fs
  .readdirSync("./src/events/")
  .filter((file) => file.endsWith(".js"));
eventFiles.forEach((file) => {
  const event = require(`./events/${file}`);
  client.on(event.name, event.execute.bind(null, client));
});

// Load process events (generic error handler)
const processFiles = fs
  .readdirSync("./src/handleErrors/")
  .filter((file) => file.endsWith(".js"));
processFiles.forEach((file) => {
  const event = require(`./handleErrors/${file}`);
  process.on(event.name, event.execute.bind(null, client));
});

// Start the bot
client.login(process.env.DISCORD_TOKEN);
