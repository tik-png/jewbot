const { bold, italic } = require("@discordjs/builders");

const getValueFromMessage = (message) => {
  const match = message.content.match(/\s\d+[\.,]?\d*/g);
  return match && match[0] ? parseFloat(match[0].replace(",", ".")) : null;
};

const getDescriptionFromMessage = (message) => {
  const value = getValueFromMessage(message);
  const mentions = Array.from(message.mentions.users.keys());

  if (!value) return;

  let description = message.content.replace(/^\.[^\s]+/g, '');     // remove the command
  mentions.forEach((mention, _) => {                               // remove mentions
    description = description.replace("<@" + mention + ">", '');
  });
  const valueReg = value.toString().replace(".", "[\\.,]") + "0?€?"; // remove the value 
  description = description.replace(new RegExp('(?<!' + valueReg + '.*?)' + valueReg, 'g'), '');
  description = description.replace(/\s\s+/g, ' ').trim();         // remove extra white spaces

  return description;
};

const getUserNickname = (userId, message) => {
  const member = message.guild.members.cache.find(
    (member) => member.id === userId
  );
  if (member) return member.nickname || member.user.username;
  else return "<@" + userId + ">"; // if not in cache then it's no longer a guild member, send the user mention back instead
};

const formatMessage = (from, connector, amount, to, value, description) => {
  let msg = "▫️ ";

  msg += bold(from);
  msg += ` ${connector} `;
  msg += bold(`${amount}€`) + " a ";
  msg += bold(to);
  msg += value ? italic(` (+${value}€)`) + "\n" : "\n";

  if (description) msg += `\xa0\xa0\xa0↳ ${italic(`"${description}"`)}\n`;

  return msg;
};

const isValidURL = (url) => {
  let check;

  try {
    check = new URL(url);
  } catch (_e) {
    return false;
  }

  return true;
};

module.exports = {
  getValueFromMessage,
  getDescriptionFromMessage,
  getUserNickname,
  formatMessage,
  isValidURL,
};
