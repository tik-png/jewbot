const { MongoClient } = require("mongodb");

/**
 * The database interface Class.
 *
 * Keywords:
 *  - creditor: A user lending a value.
 *  - debtor: A user receiving a lended sum, thus owing a value.
 */
class Bank {
  dbname = process.env.DB_NAME;

  debtcol = "debts";
  paymentlogcol = "paymentlogs";
  debtlogcol = "debtlogs";
  usercol = "users";

  /**
   * Class constructor.
   * Creates a new MongoClient and sets it to the class' `client` property.
   *
   * @param { string } connection_string The MongoDB connection string
   */
  constructor(connection_string) {
    const client = new MongoClient(connection_string, {
      useUnifiedTopology: true
    });
    this.client = client;
  }

  /**
   * Establishes the connection to the database client.
   */
  async connect() {
    await this.client.connect();
    this.db = this.client.db(this.dbname);
    console.log("✅ Database connection successful!");
  }

  /**
   * Makes a payment from one user to another.
   *
   * @param { string } from The user making the payment
   * @param { string } to The user receiving the payment
   * @param { number } value The payment value
   */
  async makePayment(from, to, value, description) {
    await this.logPayment(from, to, value, description);
    return await this.transaction(from, to, value, description);
  }

  /**
   * Gets all users that are owing money to the given user.
   *
   * @param { string } id The user to check who's owing
   */
  async getDebtors(id) {
    let query = { creditor: id, amount: { $gt: 0 } };
    return await this.db.collection(this.debtcol).find(query).toArray();
  }

  /**
   * Gets all debts of the given user.
   *
   * @param { string } id The user to check the debts.
   */
  async getDebts(id) {
    let query = { debtor: id, amount: { $gt: 0 } };
    return await this.db.collection(this.debtcol).find(query).toArray();
  }

  /**
   * Creates a new debt from one user to another.
   *
   * @param { string } creditor The user lending the value
   * @param { string } debtor The user owing the value
   * @param { number } value The debt value
   */
  async newDebt(creditor, debtor, value, description) {
    // Log the new debt
    await this.logDebt(creditor, debtor, value, description);
    return await this.transaction(creditor, debtor, value, description);
  }

  async transaction(creditor, debtor, value, description) {
    let debt = await this.getDebt(creditor, debtor);

    if (debt) {
      if (debt.creditor === creditor)
        await this.updateDebt(creditor, debtor, debt.amount + value);
      else {
        let newValue = debt.amount - value;
        if (newValue >= 0)
          await this.updateDebt(debt.creditor, debt.debtor, newValue);
        else
          await this.updateDebt(debt.debtor, debt.creditor, Math.abs(newValue));
      }
    } else await this.insertDebt(creditor, debtor, value, description);
    return await this.getDebt(creditor, debtor);
  }

  /**
   * Gets the debts between two given users.
   *
   * @param { string } id1 A user to check
   * @param { string } id2 A user to check against the first
   */
  async getDebt(id1, id2) {
    let query = {
      $or: [
        { $and: [{ creditor: id1 }, { debtor: id2 }] },
        { $and: [{ debtor: id1 }, { creditor: id2 }] }
      ]
    };
    return await this.db.collection(this.debtcol).findOne(query);
  }

  /**
   * Updates the debt values for two given users.
   *
   * @param { string } id1 A user to update
   * @param { string } id2 The second user to update
   * @param { number } value The debt value
   */
  async updateDebt(id1, id2, value) {
    value = Math.round(value * 1e2) / 1e2;
    let query = {
      $or: [
        { $and: [{ creditor: id1 }, { debtor: id2 }] },
        { $and: [{ debtor: id1 }, { creditor: id2 }] }
      ]
    };
    let set = { $set: { creditor: id1, debtor: id2, amount: value } };
    await this.db.collection(this.debtcol).updateOne(query, set);
    return true;
  }

  /**
   * Inserts a debt from a user to another.
   *
   * @param { string } creditor The user that is lending the value
   * @param { string } debtor The user that owes the value
   * @param { number } value The debt value
   */
  async insertDebt(creditor, debtor, value, description) {
    value = Math.round(value * 1e2) / 1e2;
    let query = { creditor, debtor, amount: value, description };
    await this.db.collection(this.debtcol).insertOne(query);
    return true;
  }

  /**
   * Returns all debts registed.
   */
  async getAllDebts() {
    let query = { amount: { $gt: 0 } };
    return await this.db.collection(this.debtcol).find(query).toArray();
  }

  /**
   * Gets debt logs.
   *
   * @param { number } limit The log count
   */
  async getDebtLogs(creditor = /.*/, debtor = /.*/, limit = 20, skip = 0) {
    return await this.db
      .collection(this.debtlogcol)
      .find({ creditor, debtor })
      .sort({ date: -1 })
      .skip(skip)
      .limit(limit)
      .toArray();
  }

  /**
   * Gets payment logs.
   *
   * @param { number } limit The log count
   */
  async getPaymentLogs(from = /.*/, to = /.*/,limit = 20, skip = 0) {
    return await this.db
      .collection(this.paymentlogcol)
      .find({ from, to })
      .sort({ date: -1 })
      .skip(skip)
      .limit(limit)
      .toArray();
  }

  /**
   * Creates a log of a payment.
   *
   * @param { string} from The user making the payment
   * @param { string } to The user receiving the payment
   * @param { number } value The payment value.
   */
  async logPayment(from, to, value, description) {
    let query = { from, to, amount: value, date: new Date(), description };
    await this.db.collection(this.paymentlogcol).insertOne(query);
    await this.db.collection(this.paymentlogcol).createIndex({ date: -1 });
    return true;
  }

  /**
   * Creates a log of a debt.
   *
   * @param { string } creditor The user lending the value
   * @param { string } debtor The user owing the value
   * @param { number } value The debt value
   */
  async logDebt(creditor, debtor, value, description) {
    let query = {
      creditor,
      debtor,
      amount: value,
      date: new Date(),
      description
    };
    await this.db.collection(this.debtlogcol).insertOne(query); // insert many for bulk additions
    await this.db.collection(this.debtlogcol).createIndex({ date: -1 });
    return true;
  }

  async registerUser(user, platform, link) {
    const query = { user };
    const result = await this.db.collection(this.usercol).updateOne(
      query,
      {
        $set: {
          [`platforms.${platform}`]: link
        }
      },
      {
        upsert: true
      }
    );
    return true;
  }

  async getUserLinks(user) {
    const query = { user };
    const result = await this.db.collection(this.usercol).findOne(query);
    return result?.platforms || false;
  }
}

module.exports = Bank;
