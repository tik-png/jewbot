const { ActionRowBuilder, ButtonBuilder, ButtonStyle, ComponentType, StringSelectMenuBuilder, EmbedBuilder, Colors } = require('discord.js');
const { getUserNickname } = require("./utils");
const { v4: uuidv4 } = require('uuid');

const payButton = async (client, embed, message, creditors) => {
    const msgId = message.author.id + uuidv4();
    const button = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
                .setCustomId(msgId)
                .setLabel('Pagar')
                .setStyle(ButtonStyle.Primary)
                .setEmoji('💸'),
        );

    const reply = await message.channel.send({ embeds: [embed], components: [button] });
    const collector = reply.createMessageComponentCollector({ componentType: ComponentType.Button, time: client.configs.get("messageTimeout") });
    collector.on('collect', async interaction => {
        if (interaction.customId.includes(interaction.user.id)) {
            await interaction.deferReply({ ephemeral: true });
            await payDropdown(client, message, interaction, creditors, msgId);
            collector.stop();
        } else {
            await interaction.reply({ content: `Não és o **${getUserNickname(interaction.customId.slice(0, 18), message)}** :thinking:`, ephemeral: true });
        }
    });
    collector.on('end', async _ => {
        await reply.edit({ embeds: [embed], components: [] });
    });
};

const payDropdown = async (client, message, prevItr, creditors, msgId) => {
    const dropdown = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(msgId)
                .setPlaceholder('Pagar a quem?')
                .addOptions(creditors),
        );

    const buttons = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
                .setCustomId(msgId + "|ok")
                .setStyle(ButtonStyle.Primary)
                .setEmoji('✅'),
            new ButtonBuilder()
                .setCustomId(msgId + "|notok")
                .setStyle(ButtonStyle.Primary)
                .setEmoji('❌'),
        );

    let [usr, value] = creditors[0].value.split("|");
    let reply = null;

    // Don't show the dropdown list if there is only one creditor
    if (creditors.length == 1)
        reply = await prevItr.editReply({ content: `Pagar ${value}€ a **${getUserNickname(usr, message)}**?`, components: [buttons], ephemeral: true });
    else
        reply = await prevItr.editReply({ components: [dropdown], ephemeral: true });
    const collector = reply.createMessageComponentCollector({ time: client.configs.get("messageTimeout") });

    let success = false;
    collector.on('collect', async interaction => {
        if (interaction.customId.includes(interaction.user.id)) {
            if (interaction.componentType == 3) {
                [usr, value] = interaction.values[0].split("|");
                await interaction.update({ content: `Pagar ${value}€ a **${getUserNickname(usr, message)}**?`, components: [buttons] });
            } else if (interaction.componentType == 2) {
                if (interaction.customId.split("|")[1] == "ok") {
                    await interaction.update({ content: '✅', components: [] });
                    await client.commands.get("pagar").execute(client, message, [usr], value, "");
                    success = true;
                    collector.stop();
                } else {
                    await interaction.update({ content: '', components: [dropdown] });
                }
            }
        } else {
            await interaction.deferUpdate();
            success = true;
            collector.stop();
        }
    });
    collector.on('end', async _ => {
        if (!success) {
            await prevItr.editReply({ content: 'Timeout :snail:', components: [] });
        }
    });
};

const pageEmbed = (message, purpose, list) => {
    const embed = new EmbedBuilder();
    embed.setTitle((purpose == "getDebtLogs") ? "📑 Log Dívidas" : "📑 Log Pagamentos");
    embed.setColor(Colors.Blue);
    list.slice(0, 10).reverse().forEach((entry) => {
        embed.addFields({
            name:
                entry.date.toGMTString(),
            value:
                `↘️ **${getUserNickname(
                    entry.debtor,
                    message
                )}** ${(purpose == "getDebtLogs") ? "deve" : "pagou"} a **${getUserNickname(entry.creditor, message)}** > ${entry.amount
                }€${entry.description ? ` *(${entry.description})*` : ""}\n \u200B`
        });
    });
    return embed;
}

const paginateBoard = async (client, message, purpose, userA, userB, pageSize = 10) => {
    let marker = 0;
    let logs = await client.bank[purpose](userA, userB, limit = pageSize + 1, skip = marker);
    if (logs.length == 0)
        return;
    const buttons = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
                .setCustomId("<")
                .setStyle(ButtonStyle.Secondary)
                .setEmoji('◀️')
                .setDisabled(true),
            new ButtonBuilder()
                .setCustomId(">")
                .setStyle(ButtonStyle.Secondary)
                .setEmoji('▶️')
                .setDisabled((pageSize >= logs.length) ? true : false),
        );

    const msg = await message.channel.send({ embeds: [pageEmbed(message, purpose, logs)], components: [buttons] });
    const collector = msg.createMessageComponentCollector({ componentType: ComponentType.Button, time: 2 * client.configs.get("messageTimeout") });
    collector.on('collect', async interaction => {
        if (interaction.customId == '<' && marker > 0)
            marker -= pageSize;
        else if (pageSize < logs.length)
            marker += pageSize;
        buttons.components[0].setDisabled(false);
        buttons.components[1].setDisabled(false);
        logs = await client.bank[purpose](userA, userB, limit = pageSize + 1, skip = marker);
        if (marker <= 0)
            buttons.components[0].setDisabled(true);
        if (pageSize >= logs.length)
            buttons.components[1].setDisabled(true);
        await msg.edit({ embeds: [pageEmbed(message, purpose, logs)], components: [buttons] });
        await interaction.deferUpdate();
    });
    collector.on('end', async _ => await msg.delete());
};

module.exports = {
    payButton,
    paginateBoard,
};