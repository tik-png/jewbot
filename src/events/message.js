module.exports = {
  name: "messageCreate",
  async execute(client, message) {
    const prefix = client.configs.get("prefix");
    //Ignora as mensagens que não começam com o prefixo, as mensagens de bots e as DMs.
    if (
      !message.content.startsWith(prefix) ||
      message.author.bot ||
      !message.guild
    )
      return;

    const args = message.content.slice(prefix.length).split(/\s+/);
    const cmd = args.shift().toLowerCase();
    const command =
      client.commands.get(cmd) ||
      client.commands.find(
        (command) => command.aliases && command.aliases.includes(cmd)
      );
    if (command) await command.execute(client, message);
  },
};
