const { REST, Routes } = require('discord.js');

module.exports = {
  name: "ready",
  execute(client) {
    client.user.setActivity(client.configs.get("activity"), {
      type: client.configs.get("activityType"),
    });

    // Register bot commands into all servers
    let commands = [];
    client.commands.forEach((command) => {
      if ('data' in command)
        commands.push(command.data.toJSON());
    });
    const rest = new REST({ version: "10" }).setToken(process.env.DISCORD_TOKEN);

    if (commands.length)
      client.guilds.cache.forEach((server) => {
        (async () => {
          await rest.put(
            Routes.applicationGuildCommands(client.user.id, server.id),
            { body: commands }
          );
        })();
      });

    console.info(`${client.user.username} está a funcionar`);
  },
};
