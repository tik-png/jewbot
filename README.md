# jewbot

A Discord bot to manage debts between users.

## 🚀 Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

- NodeJS 14+
- npm 6+

### Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/tik-png/jewbot
   ```

2. Install the dependencies:

   ```bash
   cd jewbot && npm install
   ```

3. [Get a Discord bot token](https://www.writebots.com/discord-bot-token/).

4. Create a `.env` file in the project's root directory. Define a `DISCORD_TOKEN` variable with your bot token:

   ```bash
   echo "DISCORD_TOKEN=[YOUR TOKEN HERE]" > .env
   ```

5. Add a new variable `DB_CONNSTRING` with your database connection string:

   ```bash
   echo "DB_CONNSTRING=[YOUR CONNECTION STRING HERE]" >> .env
   ```

After doing this, you can simply run `npm start` to start the bot.

## Available commands

- `npm start`

  Starts the bot locally. Process automatically restarts on code changes.

## Roadmap

- [x] Main logic
  - [x] Implement command logic
  - [x] Integrate with MongoDB
- [ ] Integration with payment methods
  - [x] PayPal
  - [ ] MBWay

## License

Distributed under the MIT License. See `LICENSE` for more information.
